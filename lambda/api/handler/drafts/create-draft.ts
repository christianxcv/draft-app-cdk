import { APIGatewayProxyEventV2, APIGatewayProxyResultV2, Handler } from 'aws-lambda';
import { v4 as uuid } from 'uuid';

import { addDraftToUserDrafts, createDraft } from '../../../db/data-access';
import { DraftConfig, DraftOrder } from '../../model/draft';
import { CreateDraftResponse } from '../../model/response';
import { getUsername } from '../../util/request-helper';
import { badRequest, generateJsonResponse } from '../../util/response-helper';

type CreateDraftHandler = Handler<APIGatewayProxyEventV2, APIGatewayProxyResultV2<CreateDraftResponse>>

type CreateDraftRequest = {
  draftConfig: DraftConfig
  ownerDraftPosition: number
}

export const handler: CreateDraftHandler = async (event) => {
  console.log(event)
  const username = getUsername(event)
  if (!event.body) {
    return badRequest()
  }

  const {
    draftConfig,
    ownerDraftPosition
  } = JSON.parse(event.body) as CreateDraftRequest
  draftConfig.owner = username
  draftConfig.draftName = draftConfig.draftName || `${draftConfig.numDrafters} Team Draft`

  const isDraftConfigValid = validateDraftConfig(draftConfig)
  const draftOrder = createDraftOrder(
    username,
    draftConfig.numDrafters,
    ownerDraftPosition
  )
  if (!isDraftConfigValid) {
    return badRequest()
  }

  const draftId = uuid()
  const draftDateTime = new Date().toISOString()
  const createDraftPromise = createDraft(
    draftId,
    draftDateTime,
    draftConfig,
    draftOrder
  )
  const addDraftToUserDraftsPromise = addDraftToUserDrafts(
    username,
    draftId,
    draftDateTime,
    draftConfig.draftName
  )
  await Promise.all([createDraftPromise, addDraftToUserDraftsPromise])
  return generateJsonResponse({
    body: {
      draftId,
    },
    statusCode: 201
  })
}

const validateDraftConfig = (draftConfig: DraftConfig): boolean => {
  try {
    if (draftConfig.numDrafters % 2 !== 0) {
      throw new Error()
    }
    const {
      quarterback,
      runningback,
      wideReceiver,
      tightEnd,
      flex,
      superflex,
      defense,
      kicker,
      bench,
    } = draftConfig.playerCount
    if (
      typeof (quarterback) !== 'number' ||
      typeof (runningback) !== 'number' ||
      typeof (wideReceiver) !== 'number' ||
      typeof (tightEnd) !== 'number' ||
      typeof (flex) !== 'number' ||
      typeof (superflex) !== 'number' ||
      typeof (defense) !== 'number' ||
      typeof (kicker) !== 'number' ||
      typeof (bench) !== 'number'
    ) {
      throw new Error()
    }
  } catch {
    return false
  }
  return true
}

const createDraftOrder = (userId: string, numDrafters: number, ownerDraftPosition: number): DraftOrder => {
  return Array(numDrafters).fill(null).reduce((acc, _, ind) => {
    return {
      ...acc,
      [ind + 1]: ind + 1 === ownerDraftPosition ? userId : 'CPU'
    }
  }, {})
}