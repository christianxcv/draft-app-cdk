/**
 * Use this script to transform Sleeper ADP from the Sleeper ADP Google Sheet
 * link: https://docs.google.com/spreadsheets/d/1wmjxi3K5rjIYME_lskUvquLbN331YV0vi-kg5VakpdY/edit#gid=504959809
 * 
 * 1. Download the latest sheet as CSV
 * 2. Use an online CSV to JSON converter to convert the CSV to JSON.
 * 3. Save the JSON file to ../data/sleeper-adp-raw.json
 * 4. Run the script
 */

var fs = require('fs'),
  path = require('path'),    
  filePath1 = path.join(__dirname, '../data/sleeper-adp-raw.json'),
  newFilePath = path.join(__dirname, '../out/sleeper-adp-out.json')

const rawData = fs.readFileSync(filePath1, 'utf8')

const players = JSON.parse(rawData)

const finalResult = players.reduce((acc, curr) => {
  return {
    ...acc,
    [curr["Player Id"]]: {
      playerId: curr["Player Id"],
      adp: curr["Redraft Half PPR ADP"],
      positionRank: curr["Positional Rank"]
    }
  }
}, {})

fs.writeFileSync(newFilePath, JSON.stringify(finalResult))  