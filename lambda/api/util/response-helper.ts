type JsonResponse = {
  statusCode?: number
  body?: any
}

export const generateJsonResponse = (jsonResponse: JsonResponse) => {
  return {
    statusCode: jsonResponse.statusCode || 200,
    body: jsonResponse.body ? JSON.stringify(jsonResponse.body) : undefined,
    headers: {
      'Content-Type': 'application/json'
    }
  }
}

export const generateStatusCodeResponse = (statusCode: number) => {
  return {
    statusCode
  }
}

export const success = () => generateStatusCodeResponse(200)
export const created = () => generateStatusCodeResponse(201)
export const badRequest = () => generateStatusCodeResponse(400)
export const unauthorized = () => generateStatusCodeResponse(401)
export const forbidden = () => generateStatusCodeResponse(403)
export const notFound = () => generateStatusCodeResponse(404)