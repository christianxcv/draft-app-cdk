import { APIGatewayProxyEventV2, APIGatewayProxyResultV2, Handler } from 'aws-lambda';

import { deleteDraftPick, getDraftById } from '../../../db/data-access';
import { getUsername } from '../../util/request-helper';
import { badRequest, forbidden, success } from '../../util/response-helper';


type DeleteDraftPickHandler = Handler<APIGatewayProxyEventV2, APIGatewayProxyResultV2>

export const handler: DeleteDraftPickHandler = async (event) => {
  console.log(event)
  const username = getUsername(event)
  const draftId = event.pathParameters?.draftId as string
  const pickNumberString = event.pathParameters?.pickNumber as string
  if (!validatePickNumber(pickNumberString)) {
    console.error('Invalid pick number string')
    return badRequest()
  }

  const canUserDeletePick = await validateUserCanDeletePick(username, draftId)
  if (!canUserDeletePick) {
    return forbidden()
  }

  const wasDeleteSuccessful = await deleteDraftPick(draftId, parseInt(pickNumberString))
  return wasDeleteSuccessful ? success() : badRequest()
}

const validateUserCanDeletePick = async (username: string, draftId: string): Promise<boolean> => {
  const draft = await getDraftById(draftId)
  return draft.config.owner === username
}

const validatePickNumber = (pickNumber: string): boolean => {
  return !isNaN(parseInt(pickNumber))
}