import { PreSignUpTriggerHandler } from 'aws-lambda';

import { copyPublicRanksToUserRanks } from '../../../db/data-access';

export const handler: PreSignUpTriggerHandler = async (event, _, callback) => {
  console.log(event)
  // Confirm the user
  event.response.autoConfirmUser = true;

  // Set the email as verified if it is in the request
  if (event.request.userAttributes.hasOwnProperty("email")) {
    event.response.autoVerifyEmail = true;
  }

  const username = event.userName
  await copyPublicRanksToUserRanks(username)

  callback(null, event)
}