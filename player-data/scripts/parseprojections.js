var fs = require('fs'),
  path = require('path'),    
  filePath1 = path.join(__dirname, '../out/2023-projections-out.json'),
  newFilePath = path.join(__dirname, '../out/2023-projections-transformed.json')

const rawData = fs.readFileSync(filePath1, 'utf8')

const players = JSON.parse(rawData)

const currentPositionRankCounter = {}

const result = players.map(player => {
  let playerRank = 1
  if (currentPositionRankCounter[player.position]) {
    playerRank = currentPositionRankCounter[player.position]
  } else {
    currentPositionRankCounter[player.position] = playerRank
  }
  currentPositionRankCounter[player.position] += 1

  return {
    ...player,
    positionRank: `${player.position}${playerRank}`
  }
})

// const resultAsString = JSON.stringify(result)

const finalResult = result.reduce((acc, curr) => {
  return {
    ...acc,
    [curr.playerId]: curr
  }
}, {})

fs.writeFileSync(newFilePath, JSON.stringify(finalResult))