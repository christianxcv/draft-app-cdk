import { APIGatewayProxyEventV2, APIGatewayProxyResultV2, Handler } from 'aws-lambda';

import { deleteTier } from '../../../db/data-access';
import { getUsername } from '../../util/request-helper';
import { badRequest, success } from '../../util/response-helper';

type DeleteTierHandler = Handler<APIGatewayProxyEventV2, APIGatewayProxyResultV2>

export const handler: DeleteTierHandler = async (event) => {
  console.log(event)

  const username = getUsername(event)
  const position = event.pathParameters?.position as string
  const tierId = event.pathParameters?.tierId as string

  try {
    await deleteTier(username, position, tierId)
  } catch (ex) {
    console.log(ex)
    return badRequest()
  }

  return success()
}