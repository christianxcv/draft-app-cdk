import { DraftConfig, DraftOrder, DraftPicks } from '../../api/model/draft';
import { PlayerIdToPlayerMap } from '../../api/model/player';
import { Tier, UserFlags } from '../../api/model/ranks';

export type TableItem = {
  PK: string
  SK: string
}

export interface DraftPicksItem extends TableItem {
  picks: DraftPicks
}

export interface DraftConfigItem extends TableItem {
  draftDateTime: string
  config: DraftConfig
  draftOrder: DraftOrder
}

export interface PlayersItem extends TableItem {
  players: PlayerIdToPlayerMap
}

export interface PlayerDetailItem extends TableItem {
  notes?: string
  rating?: string
}

export interface RanksItem extends TableItem {
  tiers: Tier
  tierOrder: Array<string>
  flags?: UserFlags
}