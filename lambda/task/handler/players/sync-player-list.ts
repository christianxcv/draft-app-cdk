import { GetParameterCommand, GetParameterCommandInput, SSMClient } from '@aws-sdk/client-ssm';
import axios from 'axios';

import { FANTASY_DATA_TO_SLEEPER_PLAYER_ID, SUPPORTED_POSITIONS, TEAM_LIST } from '../../../constant';
import { getAdp, getPreviousYearStats, getProjections, getStrengthOfSchedule, updatePlayerListForPosition } from '../../../db/data-access';
import { DraftLabPlayerModel, PlayerIdToPlayerMap } from '../../model/player';
import { PlayerIdToPlayerAdp, PlayerIdToPreviousYearStats, PlayerIdToProjectionMap, ProjectionsByPosition } from '../../../api/model/player';
import { StrengthOfSchedule, TeamIdToStrengthOfSchedule } from '../../../api/model/response';

const SPORTS_DATA_API_ENDPOINT = 'https://api.sportsdata.io/v3/nfl/scores/json/Players'

type SportsDataPlayerModel = {
  PlayerID: number
  Team: string
  Number: number
  FirstName: string
  LastName: string
  Position: string
  FantasyPosition: string
  Name: string
  Age: number
  PhotoUrl: string
  ByeWeek: number
  ShortName: string
  AverageDraftPosition: number
}

type DraftLabPlayerAggregate = {
  [position: string]: PlayerIdToPlayerMap
}

const ssmClient = new SSMClient({ region: 'us-east-1' })

export const handler = async () => {
  const apiKey = await getSportsDataAPIKey()
  const playerLists = await Promise.all(TEAM_LIST.map(async team => {
    const playerList = await axios.get<Array<SportsDataPlayerModel>>(`${SPORTS_DATA_API_ENDPOINT}/${team}`, {
      params: {
        key: apiKey
      }
    })
    return playerList.data
  }))
  const projections = await getProjections()
  const teamStrengthOfSchedules = await getStrengthOfSchedule()
  const previousYearStats = await getPreviousYearStats()
  const adp = await getAdp()
  const playerAggregate = aggregatePlayersByPosition(
    playerLists.flat(), 
    projections.projections, 
    teamStrengthOfSchedules.schedules,
    previousYearStats.stats,
    adp.adp
  )
  await writeEachPositionToDatabase(playerAggregate)
}

const getSportsDataAPIKey = async (): Promise<string> => {
  const params: GetParameterCommandInput = {
    Name: '/api/keys/SportsData'
  }
  const result = await ssmClient.send(new GetParameterCommand(params))
  return result.Parameter?.Value as string
}

const aggregatePlayersByPosition = (
  players: Array<SportsDataPlayerModel>, 
  projections: PlayerIdToProjectionMap,
  strengthOfSchedules: TeamIdToStrengthOfSchedule,
  previousYearStats: PlayerIdToPreviousYearStats,
  adp: PlayerIdToPlayerAdp
): DraftLabPlayerAggregate => {
  const startingAggregateObject = SUPPORTED_POSITIONS.reduce((acc: DraftLabPlayerAggregate, position: string) => {
    return {
      ...acc,
      [position]: {}
    }
  }, {})
  return players
    .filter(player => SUPPORTED_POSITIONS.includes(player.FantasyPosition) && projections[player.PlayerID])
    .map((player: SportsDataPlayerModel): DraftLabPlayerModel => {
      const playerId = player.PlayerID
      const sleeperPlayerId = FANTASY_DATA_TO_SLEEPER_PLAYER_ID[playerId]
      const x = {
        playerId,
        team: player.Team,
        number: player.Number,
        firstName: player.FirstName,
        lastName: player.LastName,
        position: player.Position,
        fantasyPosition: player.FantasyPosition,
        name: player.Name,
        age: player.Age,
        photoUrl: player.PhotoUrl,
        byeWeek: player.ByeWeek,
        shortName: player.ShortName,
        adp: sleeperPlayerId ? adp[sleeperPlayerId]?.adp : undefined,
        averageDraftPosition: adp[sleeperPlayerId],
        sos: strengthOfSchedules[player.Team][player.Position as keyof StrengthOfSchedule],
        projections: projections[playerId],
        previousYear: previousYearStats[playerId]
      }
      return x
    })
    .reduce((acc: DraftLabPlayerAggregate, player: DraftLabPlayerModel) => {
      acc[player.fantasyPosition][player.playerId] = player
      return acc
    }, startingAggregateObject)
}

const writeEachPositionToDatabase = async (playerAggregate: DraftLabPlayerAggregate) => {
  await Promise.all(Object.keys(playerAggregate).map(async position => {
    await updatePlayerListForPosition(playerAggregate[position], position)
  }))
}
