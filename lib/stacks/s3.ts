import { DnsValidatedCertificate } from '@aws-cdk/aws-certificatemanager';
import { Distribution } from '@aws-cdk/aws-cloudfront';
import { S3Origin } from '@aws-cdk/aws-cloudfront-origins';
import { User } from '@aws-cdk/aws-iam';
import { ARecord, HostedZone, RecordTarget } from '@aws-cdk/aws-route53';
import { CloudFrontTarget } from '@aws-cdk/aws-route53-targets';
import { Bucket } from '@aws-cdk/aws-s3';
import { Construct, Stack } from '@aws-cdk/core';

import { APP_PREFIX, DOMAIN_NAME } from '../constants';

export class s3Stack extends Stack {
  tiersBucket: Bucket

  constructor(scope: Construct, id: string) {
    super(scope, id)
    const staticAssetsBucket = this.createStaticAssetsBucket()
    this.createCloudFrontDistribution(staticAssetsBucket)
    this.grantGitLabAccessToPublishStaticAssets(staticAssetsBucket)
  }

  private createCloudFrontDistribution(bucket: Bucket) {
    const hostedZone = HostedZone.fromHostedZoneAttributes(this, `${APP_PREFIX}-HostedZoneLookup`, {
      hostedZoneId: 'Z0734850VURQ7DM3UPBF',
      zoneName: DOMAIN_NAME
    })
    const certificate = new DnsValidatedCertificate(this, `${APP_PREFIX}-DnsCertificate`, {
      domainName: DOMAIN_NAME,
      hostedZone
    })
    const distribution = new Distribution(this, `${APP_PREFIX}-CloudFrontDistribution`, {
      defaultBehavior: {
        origin: new S3Origin(bucket),
      },
      domainNames: [DOMAIN_NAME],
      certificate
    })
    new ARecord(this, `${APP_PREFIX}-CloudFrontAliasRecord`, {
      target: RecordTarget.fromAlias(new CloudFrontTarget(distribution)),
      zone: hostedZone
    })
  }

  private createStaticAssetsBucket(): Bucket {
    return new Bucket(this, `${APP_PREFIX}-StaticAssets`, {
      bucketName: DOMAIN_NAME,
      websiteIndexDocument: 'index.html',
      websiteErrorDocument: 'index.html',
      publicReadAccess: true
    })
  }

  private grantGitLabAccessToPublishStaticAssets(bucket: Bucket) {
    const user = User.fromUserName(this, 'GitlabUser', 'gitlab')
    bucket.grantReadWrite(user)
  }
}