import { DRAFTS, PK_PREFIX_DRAFT, PK_PREFIX_USER, PLAYERS, RANKS } from './constant';

export const buildDraftPK = (draftId: string) => `${PK_PREFIX_DRAFT}#${draftId}`
export const buildUserPK = (userId: string) => `${PK_PREFIX_USER}#${userId}`
export const buildUserDraftsPK = (userId: string) => `${buildUserPK(userId)}#${DRAFTS}`
export const buildUserPlayersPK = (userId: string) => `${buildUserPK(userId)}#${PLAYERS}`
export const buildUserRanksPK = (userId: string) => `${buildUserPK(userId)}#${RANKS}`
