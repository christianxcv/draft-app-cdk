import { DraftConfig, DraftOrder, DraftPicks, UserDraft } from './draft';
import { PlayerIdToPlayerAdp, PlayerIdToPreviousYearStats, PlayerIdToProjectionMap, PlayersByPosition, ProjectionsByPosition } from './player';
import { RanksByPosition, UserFlags } from './ranks';

export type GetDraftsResponse = {
  drafts: Array<UserDraft>
  nextPageStart?: string
}

export type GetDraftByIdResponse = {
  picks: DraftPicks
  config: DraftConfig
  draftOrder: DraftOrder
}

export type CreateDraftResponse = {
  draftId: string
}

export type GetPlayersResponse = {
  players: PlayersByPosition
}

export type GetProjectionsResponse = {
  projections: PlayerIdToProjectionMap
}

export type GetPlayerDetailResponse = {
  notes?: string
  rating?: string
}

export type GetRanksResponse = {
  ranks: RanksByPosition
  flags?: UserFlags
}

export type StrengthOfSchedule = {
  QB: number
  RB: number
  TE: number
  WR: number
}

export type TeamIdToStrengthOfSchedule = { [teamId: string]: StrengthOfSchedule }

export type GetSosResponse = {
  schedules: TeamIdToStrengthOfSchedule
}

export type GetPreviousYearStatsResponse = {
  stats: PlayerIdToPreviousYearStats
}

export type GetAdpResponse = {
  adp: PlayerIdToPlayerAdp
}