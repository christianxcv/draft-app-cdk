import { DynamoDBClient } from '@aws-sdk/client-dynamodb';
import {
  DynamoDBDocumentClient,
  GetCommand,
  GetCommandInput,
  PutCommand,
  PutCommandInput,
  QueryCommand,
  QueryCommandInput,
  UpdateCommand,
  UpdateCommandInput,
} from '@aws-sdk/lib-dynamodb';

import { DraftConfig, DraftOrder, UserDraft } from '../api/model/draft';
import { PlayersByPosition } from '../api/model/player';
import { RanksByPosition, UserFlags } from '../api/model/ranks';
import {
  CreateDraftResponse,
  GetAdpResponse,
  GetDraftByIdResponse,
  GetDraftsResponse,
  GetPlayerDetailResponse,
  GetPlayersResponse,
  GetPreviousYearStatsResponse,
  GetProjectionsResponse,
  GetRanksResponse,
  GetSosResponse,
} from '../api/model/response';
import { PlayerIdToPlayerMap } from '../task/model/player';
import { CONFIG, PICKS, PLAYERS, PROJECTIONS, PUBLIC_RANKS, RANKS, TableName } from './constant';
import { buildDraftPK, buildUserDraftsPK, buildUserPlayersPK, buildUserRanksPK } from './key-builder';
import { DraftConfigItem, DraftPicksItem, PlayerDetailItem, PlayersItem, RanksItem } from './model/db';

const client = new DynamoDBClient({ region: 'us-east-1' })
const docClient = DynamoDBDocumentClient.from(client, { marshallOptions: { removeUndefinedValues: true }})

export const getDrafts = async (
  userId: string,
  count: number | undefined = 10,
  pageStartKey: string | undefined = undefined
): Promise<GetDraftsResponse> => {
  const userPK = buildUserDraftsPK(userId)
  const params: QueryCommandInput = {
    TableName,
    KeyConditionExpression: `PK = :userPK`,
    ExpressionAttributeValues: {
      ':userPK': userPK
    },
    ProjectionExpression: 'draftId, draftName, draftDateTime',
    ScanIndexForward: false,
    Limit: count,
    ExclusiveStartKey: pageStartKey ? {
      PK: userPK,
      SK: pageStartKey
    } : undefined
  }
  const command = new QueryCommand(params)
  const res = await docClient.send(command)
  return {
    drafts: res.Items as Array<UserDraft>,
    nextPageStart: res.LastEvaluatedKey?.SK
  }
}

export const getDraftById = async (draftId: string): Promise<GetDraftByIdResponse> => {
  const draftPK = buildDraftPK(draftId)
  const params: QueryCommandInput = {
    TableName,
    KeyConditionExpression: `PK = :draftPK`,
    ExpressionAttributeValues: {
      ':draftPK': draftPK
    }
  }
  const command = new QueryCommand(params)
  const res = await docClient.send(command)

  const config = res.Items?.find(item => item.SK === CONFIG) as DraftConfigItem
  const picks = res.Items?.find(item => item.SK === PICKS) as DraftPicksItem
  return {
    picks: picks?.picks,
    config: config?.config,
    draftOrder: config?.draftOrder
  }
}

export const createDraft = async (
  draftId: string,
  draftDateTime: string,
  config: DraftConfig,
  draftOrder: DraftOrder
): Promise<CreateDraftResponse> => {
  const draftPK = buildDraftPK(draftId)

  const putConfigInput: PutCommandInput = {
    TableName,
    Item: {
      PK: draftPK,
      SK: CONFIG,
      config,
      draftOrder,
      draftDateTime,
    }
  }
  const putPicksInput: PutCommandInput = {
    TableName,
    Item: {
      PK: draftPK,
      SK: PICKS,
      picks: {}
    }
  }

  await docClient.send(new PutCommand(putConfigInput))
  await docClient.send(new PutCommand(putPicksInput))
  return {
    draftId
  }
}

export const addDraftToUserDrafts = async (
  userId: string,
  draftId: string,
  draftDateTime: string,
  draftName: string
) => {
  const userPK = buildUserDraftsPK(userId)

  const input: PutCommandInput = {
    TableName,
    Item: {
      PK: userPK,
      SK: draftDateTime,
      draftId,
      draftName,
      draftDateTime
    }
  }

  await docClient.send(new PutCommand(input))
}

export const draftPlayer = async (draftId: string, playerId: string, pickNumber: number) => {
  const draftPK = buildDraftPK(draftId)

  const input: UpdateCommandInput = {
    TableName,
    Key: {
      PK: draftPK,
      SK: PICKS
    },
    UpdateExpression: 'SET picks.#pickNumber = :playerId',
    ExpressionAttributeNames: { '#pickNumber': `${pickNumber}` },
    ExpressionAttributeValues: { ':playerId': playerId },
  }

  await docClient.send(new UpdateCommand(input))
}

export const deleteDraftPick = async (draftId: string, pickNumber: number): Promise<boolean> => {
  const draftPK = buildDraftPK(draftId)

  const input: UpdateCommandInput = {
    TableName,
    Key: {
      PK: draftPK,
      SK: PICKS
    },
    ConditionExpression: `attribute_exists(picks.#pickNumber)`,
    UpdateExpression: 'REMOVE picks.#pickNumber',
    ExpressionAttributeNames: { '#pickNumber': `${pickNumber}` },
  }

  try {
    await docClient.send(new UpdateCommand(input))
  } catch (ex) {
    console.error(ex)
    return false
  }
  return true
}

export const getAllPlayers = async (): Promise<GetPlayersResponse> => {
  const input: QueryCommandInput = {
    TableName,
    KeyConditionExpression: `PK = :playerPK`,
    ExpressionAttributeValues: {
      ':playerPK': PLAYERS
    },
  }
  const command = new QueryCommand(input)
  const res = await docClient.send(command)
  const playersByPosition = (res.Items as Array<PlayersItem>).reduce((acc: PlayersByPosition, curr) => {
    acc[curr.SK] = curr.players
    return acc
  }, {})
  return {
    players: playersByPosition
  }
}

// export const getProjections = async (): Promise<GetProjectionsResponse> => {
//   const input: QueryCommandInput = {
//     TableName,
//     KeyConditionExpression: 'PK = :projectionsPK',
//     ExpressionAttributeValues: {
//       ':projectionsPK': PROJECTIONS
//     },
//   }
//   const command = new QueryCommand(input)
//   const res = await docClient.send(command)
//   const projectionsByPosition = (res.Items as Array<any>).reduce((acc, curr) => {
//     acc[curr.SK] = curr.players
//     return acc
//   }, {})
//   return {
//     players: projectionsByPosition
//   }
// }

export const getProjections = async (): Promise<GetProjectionsResponse> => {
  const input: GetCommandInput = {
    TableName,
    Key: {
      PK: PROJECTIONS,
      SK: 'ALL'
    },
    ProjectionExpression: 'projections'
  }
  const command = new GetCommand(input)
  const res = await docClient.send(command)
  return {
    projections: res.Item?.projections
  }
}

export const getStrengthOfSchedule = async (): Promise<GetSosResponse> => {
  const input: GetCommandInput = {
    TableName,
    Key: {
      PK: 'SOS',
      SK: '2023'
    },
    ProjectionExpression: 'schedules'
  }
  const command = new GetCommand(input)
  const res = await docClient.send(command)
  return {
    schedules: res.Item?.schedules
  }
}

export const getPreviousYearStats = async (): Promise<GetPreviousYearStatsResponse> => {
  const input: GetCommandInput = {
    TableName,
    Key: {
      PK: 'PREVIOUS_YEAR',
      SK: '2022'
    },
    ProjectionExpression: 'stats'
  }
  const command = new GetCommand(input)
  const res = await docClient.send(command)
  return {
    stats: res.Item?.stats
  }
}

export const getAdp = async (): Promise<GetAdpResponse> => {
  const input: GetCommandInput = {
    TableName,
    Key: {
      PK: 'ADP',
      SK: 'ALL'
    },
    ProjectionExpression: 'adp'
  }
  const command = new GetCommand(input)
  const res = await docClient.send(command)
  return {
    adp: res.Item?.adp
  }
}

export const getPlayersByPosition = async (position: string): Promise<GetPlayersResponse> => {
  const input: GetCommandInput = {
    TableName,
    Key: {
      PK: PLAYERS,
      SK: position
    },
    ProjectionExpression: 'players'
  }

  const res = await docClient.send(new GetCommand(input))
  const item = res.Item as PlayersItem
  return {
    players: {
      [position]: item.players
    }
  }
}

export const getPlayerDetail = async (
  username: string,
  playerId: string
): Promise<GetPlayerDetailResponse> => {
  const userPlayersPk = buildUserPlayersPK(username)
  const input: GetCommandInput = {
    TableName,
    Key: {
      PK: userPlayersPk,
      SK: playerId
    },
    ProjectionExpression: 'notes, rating'
  }
  const res = await docClient.send(new GetCommand(input))
  const item = res.Item as PlayerDetailItem
  return {
    notes: item.notes,
    rating: item.rating
  }
}

export const updatePlayerDetail = async (
  username: string,
  playerId: string,
  notes?: string,
  rating?: string
) => {
  const userPlayersPK = buildUserPlayersPK(username)
  const playerSK = playerId

  const updateExpressions = []
  const expressionAttributeValues: { [key: string]: any } = {}
  if (notes) {
    updateExpressions.push('notes = :notes')
    expressionAttributeValues[':notes'] = notes
  }
  if (rating) {
    updateExpressions.push('rating = :rating')
    expressionAttributeValues[':rating'] = rating
  }
  const updateExpression = 'SET ' + updateExpressions.join(', ')

  const input: UpdateCommandInput = {
    TableName,
    Key: {
      PK: userPlayersPK,
      SK: playerSK
    },
    UpdateExpression: updateExpression,
    ExpressionAttributeValues: expressionAttributeValues,
  }
  await docClient.send(new UpdateCommand(input))
}

const addFlagsAttribute = async (
  username: string,
  position: string
) => {
  const userRanksPK = buildUserRanksPK(username)

  const input: UpdateCommandInput = {
    TableName,
    Key: {
      PK: userRanksPK,
      SK: position
    },
    UpdateExpression: 'SET flags = :emptyObject',
    ExpressionAttributeValues: {
      ':emptyObject': {}
    },
    ConditionExpression: 'attribute_not_exists(flags)'
  }
  await docClient.send(new UpdateCommand(input))
}

export const updatePlayerFlag = async (
  username: string,
  position: string,
  playerId: string,
  flag: string
) => {
  const userRanksPK = buildUserRanksPK(username)

  try {
    await addFlagsAttribute(username, position)
  } catch (ex) {
    console.log('Flags attribute already exists')
  }

  const input: UpdateCommandInput = {
    TableName,
    Key: {
      PK: userRanksPK,
      SK: position
    },
    UpdateExpression: 'SET flags.#playerId = :flag',
    ExpressionAttributeValues: {
      ':flag': flag
    },
    ExpressionAttributeNames: {
      '#playerId': playerId
    },
  }
  await docClient.send(new UpdateCommand(input))
}

export const getRanks = async (username?: string): Promise<GetRanksResponse> => {
  const input: QueryCommandInput = {
    TableName,
    KeyConditionExpression: `PK = :ranksPK`,
    ExpressionAttributeValues: {
      ':ranksPK': username ? buildUserRanksPK(username) : PUBLIC_RANKS
    },
    ProjectionExpression: 'SK, tiers, tierOrder, flags'
  }
  const res = await docClient.send(new QueryCommand(input))
  const items = res.Items as Array<RanksItem>
  const ranksByPosition = items.reduce((acc: RanksByPosition, curr) => {
    return {
      ...acc,
      [curr.SK]: {
        tierOrder: curr.tierOrder,
        tiers: curr.tiers
      }
    }
  }, {})
  const flags = items.reduce((acc: UserFlags, curr) => {
    return {
      ...acc,
      ...curr.flags
    }
  }, {})
  return {
    ranks: ranksByPosition,
    flags
  }
}

export const getRanksByPosition = async (
  position: string,
  username?: string
): Promise<GetRanksResponse> => {
  const input: GetCommandInput = {
    TableName,
    Key: {
      PK: username ? buildUserRanksPK(username) : RANKS,
      SK: position
    },
    ProjectionExpression: 'SK, tiers, tierOrder, flags'
  }
  const res = await docClient.send(new GetCommand(input))
  const ranks = res.Item as RanksItem
  return {
    ranks: {
      [ranks.SK]: {
        tiers: ranks.tiers,
        tierOrder: ranks.tierOrder
      }
    },
    flags: ranks.flags
  }
}

export const addTierToRanks = async (
  username: string,
  position: string,
  tierId: string,
  tierIndex: number
) => {
  const getCommandInput: GetCommandInput = {
    TableName,
    Key: {
      PK: buildUserRanksPK(username),
      SK: position
    },
    ProjectionExpression: 'tierOrder'
  }
  const tierOrder = await (await docClient.send(new GetCommand(getCommandInput))).Item?.tierOrder as Array<string>

  if (tierIndex > tierOrder.length + 1) {
    throw new Error('Index out of bounds')
  }

  tierOrder.splice(tierIndex, 0, tierId)

  const updateCommandInput: UpdateCommandInput = {
    TableName,
    Key: {
      PK: buildUserRanksPK(username),
      SK: position
    },
    UpdateExpression: `SET tierOrder = :tierOrder, tiers.#tierId = :emptyArray`,
    ExpressionAttributeValues: {
      ':emptyArray': [],
      ':tierOrder': tierOrder
    },
    ExpressionAttributeNames: {
      '#tierId': tierId
    },
    ConditionExpression: 'attribute_exists(tierOrder) AND attribute_exists(tiers)',
  }
  await docClient.send(new UpdateCommand(updateCommandInput))
}

export const deleteTier = async (
  username: string,
  position: string,
  tierId: string
) => {
  const getCommandInput: GetCommandInput = {
    TableName,
    Key: {
      PK: buildUserRanksPK(username),
      SK: position
    },
    ProjectionExpression: 'tierOrder'
  }
  const existingRanks = await (await docClient.send(new GetCommand(getCommandInput))).Item
  const tierOrder = existingRanks?.tierOrder as Array<string>

  const newTierOrder = tierOrder.filter(id => id !== tierId)

  const updateCommandInput: UpdateCommandInput = {
    TableName,
    Key: {
      PK: buildUserRanksPK(username),
      SK: position
    },
    UpdateExpression: `SET tierOrder = :tierOrder REMOVE tiers.#tierId`,
    ExpressionAttributeValues: {
      ':tierOrder': newTierOrder,
      ':zero': 0
    },
    ExpressionAttributeNames: {
      '#tierId': tierId
    },
    ConditionExpression: 'size(tiers.#tierId) = :zero',
  }
  await docClient.send(new UpdateCommand(updateCommandInput))
}

export const addPlayerToTier = async (
  username: string,
  position: string,
  playerId: string,
  tierId: string,
  indexWithinTier: number
) => {
  const getCommandInput: GetCommandInput = {
    TableName,
    Key: {
      PK: buildUserRanksPK(username),
      SK: position
    },
    ProjectionExpression: 'tiers'
  }
  const tiers = await (await docClient.send(new GetCommand(getCommandInput))).Item?.tiers
  const tier = tiers[tierId] as Array<string>

  if (indexWithinTier > tier.length + 1) {
    throw new Error('Index out of bounds')
  }
  tier.splice(indexWithinTier, 0, playerId)

  const updateCommandInput: UpdateCommandInput = {
    TableName,
    Key: {
      PK: buildUserRanksPK(username),
      SK: position
    },
    UpdateExpression: 'SET tiers.#tierId = :tier',
    ExpressionAttributeNames: {
      '#tierId': tierId
    },
    ExpressionAttributeValues: {
      ':tier': tier
    },
  }
  await docClient.send(new UpdateCommand(updateCommandInput))
}

export const removePlayerFromTier = async (
  username: string,
  position: string,
  tierId: string,
  indexWithinTier: number
) => {
  const input: UpdateCommandInput = {
    TableName,
    Key: {
      PK: buildUserRanksPK(username),
      SK: position
    },
    UpdateExpression: `REMOVE tiers.#tierId[${indexWithinTier}]`,
    ExpressionAttributeNames: {
      '#tierId': tierId
    }
  }
  await docClient.send(new UpdateCommand(input))
}

export const movePlayerWithinTier = async (
  username: string,
  position: string,
  playerId: string,
  tierId: string,
  oldIndexWithinTier: number,
  newIndexWithinTier: number
) => {
  const getCommandInput: GetCommandInput = {
    TableName,
    Key: {
      PK: buildUserRanksPK(username),
      SK: position
    },
    ProjectionExpression: 'tiers'
  }
  const tiers = await (await docClient.send(new GetCommand(getCommandInput))).Item?.tiers
  const tier = tiers[tierId] as Array<string>
  tier.splice(oldIndexWithinTier, 1)
  tier.splice(newIndexWithinTier, 0, playerId)

  const updateCommandInput: UpdateCommandInput = {
    TableName,
    Key: {
      PK: buildUserRanksPK(username),
      SK: position
    },
    UpdateExpression: 'SET tiers.#tierId = :tier',
    ExpressionAttributeNames: {
      '#tierId': tierId
    },
    ExpressionAttributeValues: {
      ':tier': tier
    }
  }
  await docClient.send(new UpdateCommand(updateCommandInput))
}

export const updatePlayerListForPosition = async (players: PlayerIdToPlayerMap, position: string) => {
  const playerPK = 'PLAYERS'
  const positionSK = position

  const input: UpdateCommandInput = {
    TableName: 'DraftsTable',
    Key: {
      PK: playerPK,
      SK: positionSK
    },
    UpdateExpression: 'SET players = :players',
    ExpressionAttributeValues: { ':players': players },
  }
  await docClient.send(new UpdateCommand(input))
}

export const copyPublicRanksToUserRanks = async (username: string) => {
  const queryPublicRanksInput: QueryCommandInput = {
    TableName,
    KeyConditionExpression: `PK = :ranksPK`,
    ExpressionAttributeValues: {
      ':ranksPK': PUBLIC_RANKS
    },
  }
  const publicRanks = await docClient.send(new QueryCommand(queryPublicRanksInput))
  console.log(publicRanks.Items)
  const ranksPromises = publicRanks.Items?.map(ranksItem => {
    const ranksInput: PutCommandInput = {
      TableName,
      Item: {
        ...ranksItem,
        PK: buildUserRanksPK(username)
      }
    }
    return docClient.send(new PutCommand(ranksInput))
  })
  if (!ranksPromises) {
    return
  }
  await Promise.all(ranksPromises)
}