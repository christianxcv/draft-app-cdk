import { APIGatewayProxyEventV2, APIGatewayProxyResultV2, Handler } from 'aws-lambda';

import { updatePlayerDetail } from '../../../db/data-access';
import { getUsername } from '../../util/request-helper';
import { badRequest, success } from '../../util/response-helper';

type UpdatePlayerDetailHandler = Handler<APIGatewayProxyEventV2, APIGatewayProxyResultV2>

type UpdatePlayerDetailRequest = {
  notes?: string
  rating?: string
}

export const handler: UpdatePlayerDetailHandler = async (event) => {
  console.log(event)
  if (!event.body) {
    return badRequest()
  }

  const username = getUsername(event)
  const playerId = event.pathParameters?.playerId as string
  const {
    notes,
    rating
  } = JSON.parse(event.body) as UpdatePlayerDetailRequest
  await updatePlayerDetail(username, playerId, notes, rating)
  return success()
}