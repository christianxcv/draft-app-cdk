import { AccountRecovery, UserPool } from '@aws-cdk/aws-cognito';
import { Table } from '@aws-cdk/aws-dynamodb';
import { Function } from '@aws-cdk/aws-lambda';
import { NodejsFunction } from '@aws-cdk/aws-lambda-nodejs';
import { RetentionDays } from '@aws-cdk/aws-logs';
import { Construct, Duration, Stack } from '@aws-cdk/core';
import path = require('path');

import { APP_PREFIX } from '../constants';

type CognitoProps = {
  draftsTable: Table
}
export class CognitoStack extends Stack {
  readonly userPool: UserPool
  private preSignUpLambda: Function

  constructor(scope: Construct, id: string, props: CognitoProps) {
    super(scope, id)
    this.preSignUpLambda = this.createPreSignUpTriggerLambda(props.draftsTable)
    this.userPool = this.createUserPool()
  }

  private createPreSignUpTriggerLambda(draftsTable: Table): Function {
    const fn = new NodejsFunction(this, `${APP_PREFIX}-PreSignUpTriggerLambda`, {
      logRetention: RetentionDays.ONE_WEEK,
      timeout: Duration.seconds(15),
      functionName: 'PreSignUpTriggerLambda',
      entry: path.join(__dirname, `../../lambda/task/handler/user/pre-signup-trigger.ts`)
    })
    draftsTable.grantReadWriteData(fn)
    return fn
  }

  private createUserPool(): UserPool {
    const userPool = new UserPool(this, `${APP_PREFIX}-UserPool`, {
      userPoolName: `${APP_PREFIX}-UserPool`,
      selfSignUpEnabled: true,
      signInCaseSensitive: false,
      signInAliases: {
        username: true,
        email: true
      },
      accountRecovery: AccountRecovery.EMAIL_ONLY,
      passwordPolicy: {
        minLength: 6,
        requireDigits: false,
        requireLowercase: false,
        requireSymbols: false,
        requireUppercase: false,
      },
      lambdaTriggers: {
        preSignUp: this.preSignUpLambda,
      }
    })

    userPool.addDomain(`${APP_PREFIX}-Domain`, {
      // TODO: use a custom domain once we have that set up
      cognitoDomain: {
        domainPrefix: 'draft-app-dev'
      }
    })

    // const appClient = userPool.addClient(`${APP_PREFIX}-RestApi`, {
    //   refreshTokenValidity: Duration.days(3650),
    //   accessTokenValidity: Duration.days(1),
    //   idTokenValidity: Duration.days(1),
    //   authFlows: {
    //     userPassword: true,
    //     adminUserPassword: true
    //   },
    //   supportedIdentityProviders: [
    //     UserPoolClientIdentityProvider.COGNITO
    //   ],
    //   oAuth: {
    //     callbackUrls: ['https://example.com/callback'],
    //     logoutUrls: ['https://example.com/logout'],
    //     scopes: [
    //       OAuthScope.COGNITO_ADMIN,
    //       OAuthScope.EMAIL,
    //       OAuthScope.OPENID,
    //       OAuthScope.PHONE,
    //       OAuthScope.PROFILE,
    //     ],
    //     flows: {
    //       authorizationCodeGrant: true,
    //       implicitCodeGrant: true
    //     },
    //   },

    // })

    return userPool
  }
}