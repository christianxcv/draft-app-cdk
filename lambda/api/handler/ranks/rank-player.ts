import { APIGatewayProxyEventV2, APIGatewayProxyResultV2, Handler } from 'aws-lambda';

import { addPlayerToTier, movePlayerWithinTier, removePlayerFromTier } from '../../../db/data-access';
import { getUsername } from '../../util/request-helper';
import { badRequest, success } from '../../util/response-helper';

type RankPlayerHandler = Handler<APIGatewayProxyEventV2, APIGatewayProxyResultV2>

type RankPlayerRequest = {
  oldTierId?: string
  oldIndexWithinTier?: number
  newTierId: string
  newIndexWithinTier: number
}

/**
 * TODO: validation
 * - Validate player belongs to position they're being ranked in
 * - Validate player is not being duplicated across tiers
 */

export const handler: RankPlayerHandler = async (event) => {
  console.log(event)

  if (!event.body) {
    return badRequest()
  }

  const username = getUsername(event)
  const position = event.pathParameters?.position as string
  const playerId = event.pathParameters?.playerId as string

  const {
    oldTierId,
    oldIndexWithinTier,
    newTierId,
    newIndexWithinTier,
  } = JSON.parse(event.body) as RankPlayerRequest

  if (!oldTierId) {
    await addPlayerToTier(
      username,
      position,
      playerId,
      newTierId,
      newIndexWithinTier
    )
    return success()
  }

  if (!oldIndexWithinTier && oldIndexWithinTier !== 0) {
    return badRequest()
  }

  if (oldTierId === newTierId) {
    if (oldIndexWithinTier === newIndexWithinTier) {
      return success()
    }

    await movePlayerWithinTier(
      username,
      position,
      playerId,
      newTierId,
      oldIndexWithinTier,
      newIndexWithinTier
    )
    return success()
  }

  await addPlayerToTier(
    username,
    position,
    playerId,
    newTierId,
    newIndexWithinTier
  )
  await removePlayerFromTier(
    username,
    position,
    oldTierId,
    oldIndexWithinTier,
  )
  return success()
}