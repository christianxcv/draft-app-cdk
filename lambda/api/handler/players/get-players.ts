import { APIGatewayProxyEventV2, APIGatewayProxyResultV2, Handler } from 'aws-lambda';

import { SUPPORTED_POSITIONS } from '../../../constant';
import { getAllPlayers, getPlayersByPosition } from '../../../db/data-access';
import { GetPlayersResponse } from '../../model/response';
import { extractOptionStringParamFromQueryParams } from '../../util/request-helper';
import { generateJsonResponse } from '../../util/response-helper';

type GetPlayersHandler = Handler<APIGatewayProxyEventV2, APIGatewayProxyResultV2<GetPlayersResponse>>

export const handler: GetPlayersHandler = async (event) => {
  console.log(event)
  const position = extractOptionStringParamFromQueryParams(event, 'position')
  const filter = extractOptionStringParamFromQueryParams(event, 'filter')
  const response = position && SUPPORTED_POSITIONS.includes(position)
    ? await getPlayersByPosition(position)
    : await getAllPlayers()

  /**
   * If the filter value is not "ALL", only return players who have an ADP
   */
  if (filter !== 'ALL') {
    Object.keys(response.players)
      .map(key => response.players[key])
      .forEach(playerIdToPlayer => {
        Object.keys(playerIdToPlayer).forEach(playerId => {
          if (!playerIdToPlayer[playerId].adp) {
            delete playerIdToPlayer[playerId]
          }
        })
      })
  }

  return generateJsonResponse({
    body: response
  })
}
