import { APIGatewayProxyEventV2, APIGatewayProxyResultV2, Handler } from 'aws-lambda';

import { updatePlayerFlag } from '../../../db/data-access';
import { getUsername } from '../../util/request-helper';
import { badRequest, success } from '../../util/response-helper';

type UpdatePlayerFlagHandler = Handler<APIGatewayProxyEventV2, APIGatewayProxyResultV2>

type UpdatePlayerFlagRequest = {
  flag: string
}

export const handler: UpdatePlayerFlagHandler = async (event) => {
  console.log(event)
  if (!event.body) {
    return badRequest()
  }

  const username = getUsername(event)
  const position = event.pathParameters?.position as string
  const playerId = event.pathParameters?.playerId as string
  const {
    flag
  } = JSON.parse(event.body) as UpdatePlayerFlagRequest
  await updatePlayerFlag(username, position, playerId, flag)
  return success()
}