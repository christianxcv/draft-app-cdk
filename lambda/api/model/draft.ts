export type UserDraft = {
  draftId: string
  draftDateTime: string
  draftName: string
}

export type DraftOrder = {
  [draftPos: number]: string
}

export type DraftPicks = {
  [pickNum: number]: string
}

export type PlayerCount = {
  quarterback: number
  runningback: number
  wideReceiver: number
  tightEnd: number
  flex: number
  superflex: number
  defense: number
  kicker: number
  bench: number
}

export type DraftConfig = {
  numDrafters: number
  playerCount: PlayerCount
  draftName?: string
  owner?: string
}