import { APIGatewayProxyEventV2, APIGatewayProxyResultV2, Handler } from 'aws-lambda';

import { getDraftById } from '../../../db/data-access';
import { GetDraftByIdResponse } from '../../model/response';
import { generateJsonResponse } from '../../util/response-helper';

type GetDraftByIdHandler = Handler<APIGatewayProxyEventV2, APIGatewayProxyResultV2<GetDraftByIdResponse>>

export const handler: GetDraftByIdHandler = async (event) => {
  console.log(event)
  const draftId = event.pathParameters?.draftId as string
  const response = await getDraftById(draftId)
  console.log(response)
  return generateJsonResponse({
    body: response
  })
}