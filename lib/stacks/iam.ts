import { User } from '@aws-cdk/aws-iam';
import { Construct, Stack } from '@aws-cdk/core';

import { APP_PREFIX } from '../constants';

export class iamStack extends Stack {
  constructor(scope: Construct, id: string) {
    super(scope, id)
    this.createGitlabUser()
  }

  private createGitlabUser() {
    new User(this, `${APP_PREFIX}-GitlabUser`, {
      userName: 'gitlab'
    })
  }
}