import { APIGatewayProxyEventV2, APIGatewayProxyResultV2, Handler } from 'aws-lambda';

import { getUsername } from '../../util/request-helper';
import { badRequest, success } from '../../util/response-helper';

type UpdateUserRanksHandler = Handler<APIGatewayProxyEventV2, APIGatewayProxyResultV2>;

enum UpdateUserRanksOperationType {
  AddTier = "ADD_TIER",
  RemoveTier = "REMOVE_TIER",
  MovePlayer = "MOVE_PLAYER"
}

type AddTierRequest = {
  tierId: string
  tierIndex: number
}

type MovePlayerRequest = {
  oldTierId: string
  oldIndexWithinTier: number
  newTierId: string
  newIndexWithinTier: number
}

type DeleteTierRequest = {
  tierId: string
}

type UpdateUserRanksRequest = {
  operationType: UpdateUserRanksOperationType

}

export const handler: UpdateUserRanksHandler = async (event) => {
  console.log(event)
  if (!event.body) {
    return badRequest()
  }

  const username = getUsername(event)
  const position = event.pathParameters?.position as string


  return success()
}