/**
 * Use this script to transform the list of player data from the joined FantasyData tables into 
 * an object that is keyed by playerId
 * 
 * 1. First run the joinCSVs script to join the sub-tables from FantasyData
 * 2. Then run this script to transform the list to an object that can be saved to Dynamo
 */

var fs = require('fs'),
  path = require('path'),    
  filePath1 = path.join(__dirname, '../out/2022-results-out.json'),
  newFilePath = path.join(__dirname, '../out/2022-results-transformed.json')

const rawData = fs.readFileSync(filePath1, 'utf8')

const players = JSON.parse(rawData)

const currentPositionRankCounter = {}

const result = players.map(player => {
  let playerRank = 1
  if (currentPositionRankCounter[player.position]) {
    playerRank = currentPositionRankCounter[player.position]
  } else {
    currentPositionRankCounter[player.position] = playerRank
  }
  currentPositionRankCounter[player.position] += 1

  return {
    gamesPlayed: player.gamesPlayed,
    ppg: player.ppg,
    fPts: player.fPts,
    playerId: player.playerId,
    positionRank: `${player.position}${playerRank}`
  }
})

// const resultAsString = JSON.stringify(result)

const finalResult = result.reduce((acc, curr) => {
  return {
    ...acc,
    [curr.playerId]: curr
  }
}, {})

fs.writeFileSync(newFilePath, JSON.stringify(finalResult))