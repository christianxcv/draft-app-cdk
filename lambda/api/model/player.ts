export type Player = {
  playerId: number
  team: string
  number: number
  firstName: string
  lastName: string
  position: string
  fantasyPosition: string
  name: string
  age: number
  photoUrl: string
  byeWeek: number
  shortName: string
  adp: number
  sos: number
  averageDraftPosition?: PlayerAdp
  projections?: Projection
  previousYear?: PreviousYearStats
}

export type PlayerIdToPlayerMap = { [playerId: string]: Player }

export type PlayersByPosition = {
  [position: string]: PlayerIdToPlayerMap
}

export type Projection = {
  fPts: number
  gamesPlayed: number
  name: string
  passAtt: number
  passComp: number
  passInt: number
  passTd: number
  passYd: number
  playerId: number
  position: string
  positionRank: string
  ppg: number
  recRec: number
  recTd: number
  recTgt: number
  recYd: number
  ruAtt: number
  ruTd: number
  ruYd: number
  team: string
}

export type PlayerIdToProjectionMap = { [playerId: string ]: Projection }

export type ProjectionsByPosition = {
  [position: string]: Array<Projection>
}

export type PreviousYearStats = {
  fPts: number
  gamesPlayed: number 
  playerId: number
  positionRank: string
  ppg: number
}

export type PlayerIdToPreviousYearStats = { [playerId: string]: PreviousYearStats }

export type PlayerAdp = {
  adp: number
  positionRank: string
}

export type PlayerIdToPlayerAdp = { [playerId: string]: PlayerAdp }