import { Table } from '@aws-cdk/aws-dynamodb';
import { Rule, Schedule } from '@aws-cdk/aws-events';
import { LambdaFunction } from '@aws-cdk/aws-events-targets';
import { Policy, PolicyStatement } from '@aws-cdk/aws-iam';
import { Function } from '@aws-cdk/aws-lambda';
import { NodejsFunction, NodejsFunctionProps } from '@aws-cdk/aws-lambda-nodejs';
import { RetentionDays } from '@aws-cdk/aws-logs';
import { Construct, Duration, Stack } from '@aws-cdk/core';
import * as path from 'path';

import { APP_PREFIX } from '../constants';

type LambdaProps = {
  draftsTable: Table
}

const DEFAULT_LAMBDA_OPTIONS: NodejsFunctionProps = {
  logRetention: RetentionDays.ONE_WEEK,
  timeout: Duration.seconds(5),
}

export class LambdaStack extends Stack {
  private readonly draftsTable: Table

  readonly getDraftsLambdaFn: Function
  readonly getDraftByIdLambdaFn: Function
  readonly createDraftLambdaFn: Function
  readonly draftPlayerLambdaFn: Function
  readonly deleteDraftPickLambdaFn: Function
  readonly getPlayersLambdaFn: Function
  readonly getPlayerDetailLambdaFn: Function
  readonly updatePlayerDetailLambdaFn: Function
  readonly getUserRanksLambdaFn: Function
  readonly getPublicRanksLambdaFn: Function
  readonly addTierToRanksLambdaFn: Function
  readonly rankPlayerLambdaFn: Function
  readonly updatePlayerFlagLambdaFn: Function
  readonly deleteTierLambdaFn: Function

  constructor(scope: Construct, id: string, props: LambdaProps) {
    super(scope, id)
    this.draftsTable = props.draftsTable

    this.getDraftsLambdaFn = this.createGetDraftsLambda()
    this.createDraftLambdaFn = this.createCreateDraftLambda()
    this.getDraftByIdLambdaFn = this.createGetDraftByIdLambda()
    this.draftPlayerLambdaFn = this.createDraftPlayerLambda()
    this.deleteDraftPickLambdaFn = this.createDeleteDraftPickLambda()
    this.getPlayersLambdaFn = this.createGetPlayersLambda()
    this.getPlayerDetailLambdaFn = this.createGetPlayerDetailLambda()
    this.updatePlayerDetailLambdaFn = this.createUpdatePlayerDetailLambda()
    this.getUserRanksLambdaFn = this.createGetUserRanksLambda()
    this.getPublicRanksLambdaFn = this.createGetPublicRanksLambda()
    this.addTierToRanksLambdaFn = this.createAddTierToRanksLambda()
    this.rankPlayerLambdaFn = this.createRankPlayerLambda()
    this.updatePlayerFlagLambdaFn = this.createUpdatePlayerFlagLambda()
    this.deleteTierLambdaFn = this.createDeleteTierLambda()

    this.createSyncPlayerListTaskLambdaSchedule()
  }

  private createGetDraftsLambda(): Function {
    const fn = new NodejsFunction(this, `${APP_PREFIX}-GetDraftsLambda`, {
      ...DEFAULT_LAMBDA_OPTIONS,
      functionName: 'GetDraftsLambda',
      entry: this.generateApiFunctionPath('drafts/get-drafts'),
    })
    this.draftsTable.grantReadData(fn)
    return fn
  }

  private createCreateDraftLambda(): Function {
    const fn = new NodejsFunction(this, `${APP_PREFIX}-CreateDraftLambda`, {
      ...DEFAULT_LAMBDA_OPTIONS,
      functionName: 'CreateDraftLambda',
      entry: this.generateApiFunctionPath('drafts/create-draft'),
    })
    this.draftsTable.grantReadWriteData(fn)
    return fn
  }

  private createGetDraftByIdLambda(): Function {
    const fn = new NodejsFunction(this, `${APP_PREFIX}-GetDraftByIdLambda`, {
      ...DEFAULT_LAMBDA_OPTIONS,
      functionName: 'GetDraftByIdLambda',
      entry: this.generateApiFunctionPath('drafts/get-draft-by-id'),
    })
    this.draftsTable.grantReadData(fn)
    return fn
  }

  private createDraftPlayerLambda(): Function {
    const fn = new NodejsFunction(this, `${APP_PREFIX}-DraftPlayerLambda`, {
      ...DEFAULT_LAMBDA_OPTIONS,
      functionName: 'DraftPlayerLambda',
      entry: this.generateApiFunctionPath('drafts/draft-player'),
    })
    this.draftsTable.grantReadWriteData(fn)
    return fn
  }

  private createDeleteDraftPickLambda(): Function {
    const fn = new NodejsFunction(this, `${APP_PREFIX}-DeleteDraftPickLambda`, {
      ...DEFAULT_LAMBDA_OPTIONS,
      functionName: 'DeleteDraftPickLambda',
      entry: this.generateApiFunctionPath('drafts/delete-draft-pick'),
    })
    this.draftsTable.grantReadWriteData(fn)
    return fn
  }

  private createGetPlayersLambda(): Function {
    const fn = new NodejsFunction(this, `${APP_PREFIX}-GetPlayersLambda`, {
      ...DEFAULT_LAMBDA_OPTIONS,
      functionName: 'GetPlayersLambda',
      entry: this.generateApiFunctionPath('players/get-players'),
      memorySize: 1024
    })
    this.draftsTable.grantReadData(fn)
    return fn
  }

  private createGetPlayerDetailLambda(): Function {
    const fn = new NodejsFunction(this, `${APP_PREFIX}-GetPlayerDetailLambda`, {
      ...DEFAULT_LAMBDA_OPTIONS,
      functionName: 'GetPlayerDetailLambda',
      entry: this.generateApiFunctionPath('players/get-player-detail')
    })
    this.draftsTable.grantReadData(fn)
    return fn
  }

  private createUpdatePlayerDetailLambda(): Function {
    const fn = new NodejsFunction(this, `${APP_PREFIX}-UpdatePlayerDetailLambda`, {
      ...DEFAULT_LAMBDA_OPTIONS,
      functionName: 'UpdatePlayerDetailLambda',
      entry: this.generateApiFunctionPath('players/update-player-detail')
    })
    this.draftsTable.grantReadWriteData(fn)
    return fn
  }

  private createGetUserRanksLambda(): Function {
    const fn = new NodejsFunction(this, `${APP_PREFIX}-GetUserRanksLambda`, {
      ...DEFAULT_LAMBDA_OPTIONS,
      functionName: 'GetUserRanksLambda',
      entry: this.generateApiFunctionPath('ranks/get-user-ranks'),
      memorySize: 1024,
    })
    this.draftsTable.grantReadData(fn)
    return fn
  }

  private createGetPublicRanksLambda(): Function {
    const fn = new NodejsFunction(this, `${APP_PREFIX}-GetPublicRanksLambda`, {
      ...DEFAULT_LAMBDA_OPTIONS,
      functionName: 'GetPublicRanksLambda',
      entry: this.generateApiFunctionPath('ranks/get-public-ranks'),
      memorySize: 1024,
    })
    this.draftsTable.grantReadData(fn)
    return fn
  }

  private createAddTierToRanksLambda(): Function {
    const fn = new NodejsFunction(this, `${APP_PREFIX}-AddTierToRanksLambda`, {
      ...DEFAULT_LAMBDA_OPTIONS,
      functionName: 'AddTierToRanksLambda',
      entry: this.generateApiFunctionPath('ranks/add-tier-to-ranks'),
    })
    this.draftsTable.grantReadWriteData(fn)
    return fn
  }

  private createDeleteTierLambda(): Function {
    const fn = new NodejsFunction(this, `${APP_PREFIX}-DeleteTierLambda`, {
      ...DEFAULT_LAMBDA_OPTIONS,
      functionName: 'DeleteTierLambda',
      entry: this.generateApiFunctionPath('ranks/delete-tier'),
    })
    this.draftsTable.grantReadWriteData(fn)
    return fn
  }

  private createRankPlayerLambda(): Function {
    const fn = new NodejsFunction(this, `${APP_PREFIX}-RankPlayerLambda`, {
      ...DEFAULT_LAMBDA_OPTIONS,
      functionName: 'RankPlayerLambda',
      entry: this.generateApiFunctionPath('ranks/rank-player')
    })
    this.draftsTable.grantReadWriteData(fn)
    return fn
  }

  private createUpdatePlayerFlagLambda(): Function {
    const fn = new NodejsFunction(this, `${APP_PREFIX}-UpdatePlayerFlagLambda`, {
      ...DEFAULT_LAMBDA_OPTIONS,
      functionName: 'UpdatePlayerFlagLambda',
      entry: this.generateApiFunctionPath('ranks/update-player-flag')
    })
    this.draftsTable.grantReadWriteData(fn)
    return fn
  }

  private createSyncPlayerListTaskLambdaSchedule(): Function {
    const fn = new NodejsFunction(this, `${APP_PREFIX}-SyncPlayerListLambda`, {
      ...DEFAULT_LAMBDA_OPTIONS,
      functionName: 'SyncPlayerListTaskLambda',
      entry: this.generateTaskFunctionPath('players/sync-player-list'),
      timeout: Duration.seconds(30)
    })
    this.draftsTable.grantReadWriteData(fn)

    fn.role?.attachInlinePolicy(new Policy(this, `${APP_PREFIX}-LambdaSsmPolicy`, {
      policyName: 'GetSportsDataApiKeyParameter',
      statements: [
        new PolicyStatement({
          resources: ['arn:aws:ssm:us-east-1:383989225973:parameter/api/keys/SportsData'],
          actions: ['ssm:GetParameter'],
        })
      ]
    }))

    new Rule(this, `${APP_PREFIX}-SyncPlayerListCronRule`, {
      schedule: Schedule.cron({
        minute: '0',
        hour: '0',
        weekDay: '1',
      }),
      targets: [new LambdaFunction(fn)]
    })

    return fn
  }

  private generateApiFunctionPath(functionName: string) {
    return path.join(__dirname, `../../lambda/api/handler/${functionName}.ts`)
  }

  private generateTaskFunctionPath(functionName: string) {
    return path.join(__dirname, `../../lambda/task/handler/${functionName}.ts`)
  }
}