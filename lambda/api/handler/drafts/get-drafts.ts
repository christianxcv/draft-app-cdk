import { APIGatewayProxyEventV2, APIGatewayProxyResultV2, Handler } from 'aws-lambda';

import { getDrafts } from '../../../db/data-access';
import { GetDraftsResponse } from '../../model/response';
import {
  extractOptionalNumberParamFromQueryParams,
  extractOptionStringParamFromQueryParams,
  getUsername,
} from '../../util/request-helper';
import { generateJsonResponse } from '../../util/response-helper';

type GetDraftsHandler = Handler<APIGatewayProxyEventV2, APIGatewayProxyResultV2<GetDraftsResponse>>

export const handler: GetDraftsHandler = async (event) => {
  console.log(event)
  const username = getUsername(event)
  const count = extractOptionalNumberParamFromQueryParams(event, 'count')
  const start = extractOptionStringParamFromQueryParams(event, 'start')

  const response = await getDrafts(username, count, start)
  console.log(response)
  return generateJsonResponse({
    body: response
  })
}
