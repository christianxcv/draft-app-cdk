import { CorsHttpMethod, HttpApi, HttpMethod } from '@aws-cdk/aws-apigatewayv2';
import { HttpUserPoolAuthorizer } from '@aws-cdk/aws-apigatewayv2-authorizers';
import { HttpLambdaIntegration } from '@aws-cdk/aws-apigatewayv2-integrations';
import { UserPool } from '@aws-cdk/aws-cognito';
import { Construct, Duration, Stack, StackProps } from '@aws-cdk/core';

import { APP_PREFIX, DOMAIN_NAME } from '../constants';
import { LambdaStack } from './lambda';

export interface RestApiStackProps extends StackProps {
  lambdaStack: LambdaStack
  userPool: UserPool
}

export class RestApiStack extends Stack {
  private readonly userPoolAuthorizer: HttpUserPoolAuthorizer
  private readonly lambdaStack: LambdaStack

  constructor(scope: Construct, id: string, props: RestApiStackProps) {
    super(scope, id)
    this.lambdaStack = props.lambdaStack
    this.userPoolAuthorizer = this.createUserPoolAuthorizer(props.userPool)
    this.createApi()
  }

  private createApi() {
    const httpApi = new HttpApi(this, `${APP_PREFIX}-HttpApi`, {
      apiName: `${APP_PREFIX}-HttpApi`,
      corsPreflight: {
        allowHeaders: [
          'Authorization',
          'Content-Type'
        ],
        allowMethods: [
          CorsHttpMethod.OPTIONS,
          CorsHttpMethod.GET,
          CorsHttpMethod.POST,
          CorsHttpMethod.PUT,
          CorsHttpMethod.PATCH,
          CorsHttpMethod.DELETE,
        ],
        allowOrigins: [
          'http://localhost:3000',
          `https://${DOMAIN_NAME}`
        ],
        maxAge: Duration.minutes(10),
      }
    })

    const getDraftsLambdaIntegration = new HttpLambdaIntegration('GetDraftsLambdaIntegration', this.lambdaStack.getDraftsLambdaFn)
    httpApi.addRoutes({
      path: '/api/drafts',
      methods: [HttpMethod.GET],
      integration: getDraftsLambdaIntegration,
      authorizer: this.userPoolAuthorizer
    })

    const createDraftLambdaIntegration = new HttpLambdaIntegration('CreateDraftLambdaIntegration', this.lambdaStack.createDraftLambdaFn)
    httpApi.addRoutes({
      path: '/api/drafts',
      methods: [HttpMethod.POST],
      integration: createDraftLambdaIntegration,
      authorizer: this.userPoolAuthorizer,
    })

    const getDraftByIdLambdaIntegration = new HttpLambdaIntegration('GetDraftByIdLambdaIntegration', this.lambdaStack.getDraftByIdLambdaFn)
    httpApi.addRoutes({
      path: '/api/drafts/{draftId}',
      methods: [HttpMethod.GET],
      integration: getDraftByIdLambdaIntegration,
    })

    const draftPlayerLambdaIntegration = new HttpLambdaIntegration('DraftPlayerLambdaIntegration', this.lambdaStack.draftPlayerLambdaFn)
    httpApi.addRoutes({
      path: '/api/drafts/{draftId}/picks',
      methods: [HttpMethod.POST],
      integration: draftPlayerLambdaIntegration,
      authorizer: this.userPoolAuthorizer,
    })

    const deleteDraftPickLambdaIntegration = new HttpLambdaIntegration('DeleteDraftPickLambdaIntegration', this.lambdaStack.deleteDraftPickLambdaFn)
    httpApi.addRoutes({
      path: '/api/drafts/{draftId}/picks/{pickNumber}',
      methods: [HttpMethod.DELETE],
      integration: deleteDraftPickLambdaIntegration,
      authorizer: this.userPoolAuthorizer,
    })

    const getPlayersLambdaIntegration = new HttpLambdaIntegration('GetPlayersLambdaIntegration', this.lambdaStack.getPlayersLambdaFn)
    httpApi.addRoutes({
      path: '/api/players',
      methods: [HttpMethod.GET],
      integration: getPlayersLambdaIntegration,
    })

    const getPlayerDetailLambdaIntegration = new HttpLambdaIntegration('GetPlayerDetailLambdaIntegration', this.lambdaStack.getPlayerDetailLambdaFn)
    httpApi.addRoutes({
      path: '/api/players/{playerId}',
      methods: [HttpMethod.GET],
      integration: getPlayerDetailLambdaIntegration,
      authorizer: this.userPoolAuthorizer
    })

    const updatePlayerDetailLambdaIntegration = new HttpLambdaIntegration('UpdatePlayerDetailLambdaIntegration', this.lambdaStack.updatePlayerDetailLambdaFn)
    httpApi.addRoutes({
      path: '/api/players/{playerId}',
      methods: [HttpMethod.PUT],
      integration: updatePlayerDetailLambdaIntegration,
      authorizer: this.userPoolAuthorizer,
    })

    const getUserRanksLambdaIntegration = new HttpLambdaIntegration('GetUserRanksLambdaIntegration', this.lambdaStack.getUserRanksLambdaFn)
    httpApi.addRoutes({
      path: '/api/ranks',
      methods: [HttpMethod.GET],
      integration: getUserRanksLambdaIntegration,
      authorizer: this.userPoolAuthorizer,
    })

    const getPublicRanksLambdaIntegration = new HttpLambdaIntegration('GetPublicRanksLambdaIntegration', this.lambdaStack.getPublicRanksLambdaFn)
    httpApi.addRoutes({
      path: '/api/public/ranks',
      methods: [HttpMethod.GET],
      integration: getPublicRanksLambdaIntegration,
    })

    const addTierToRanksLambdaIntegration = new HttpLambdaIntegration('AddTierToRanksLambdaIntegration', this.lambdaStack.addTierToRanksLambdaFn)
    httpApi.addRoutes({
      path: '/api/ranks/{position}',
      methods: [HttpMethod.PUT],
      integration: addTierToRanksLambdaIntegration,
      authorizer: this.userPoolAuthorizer,
    })

    const deleteTierLambdaIntegration = new HttpLambdaIntegration('DeleteTierLambdaIntegration', this.lambdaStack.deleteTierLambdaFn)
    httpApi.addRoutes({
      path: '/api/ranks/{position}/tiers/{tierId}',
      methods: [HttpMethod.DELETE],
      integration: deleteTierLambdaIntegration,
      authorizer: this.userPoolAuthorizer,
    })

    const rankPlayerLambdaIntegration = new HttpLambdaIntegration('RankPlayerLambdaIntegration', this.lambdaStack.rankPlayerLambdaFn)
    httpApi.addRoutes({
      path: '/api/ranks/{position}/players/{playerId}',
      methods: [HttpMethod.PUT],
      integration: rankPlayerLambdaIntegration,
      authorizer: this.userPoolAuthorizer
    })

    const updatePlayerFlagLambdaIntegration = new HttpLambdaIntegration('UpdatePlayerLambdaIntegration', this.lambdaStack.updatePlayerFlagLambdaFn)
    httpApi.addRoutes({
      path: '/api/ranks/{position}/players/{playerId}/flags',
      methods: [HttpMethod.PUT],
      integration: updatePlayerFlagLambdaIntegration,
      authorizer: this.userPoolAuthorizer
    })
  }

  private createUserPoolAuthorizer(userPool: UserPool) {
    return new HttpUserPoolAuthorizer(`${APP_PREFIX}-UserPoolAuthorizer`, userPool)
  }
}