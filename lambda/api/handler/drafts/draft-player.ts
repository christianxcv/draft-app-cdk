import { APIGatewayProxyEventV2, APIGatewayProxyResultV2, Handler } from 'aws-lambda';

import { draftPlayer } from '../../../db/data-access';
import { badRequest, created } from '../../util/response-helper';

type DraftPlayerHandler = Handler<APIGatewayProxyEventV2, APIGatewayProxyResultV2>

type DraftPlayerRequest = {
  playerId: string
  pickNumber: number
}

export const handler: DraftPlayerHandler = async (event) => {
  console.log(event)
  if (!event.body) {
    return badRequest()
  }

  const { pickNumber, playerId } = JSON.parse(event.body) as DraftPlayerRequest
  const draftId = event.pathParameters?.draftId as string
  await draftPlayer(draftId, playerId, pickNumber)
  return created()
}