import { APIGatewayProxyEventV2, APIGatewayProxyResultV2, Handler } from 'aws-lambda';
import { v4 as uuid } from 'uuid';

import { addTierToRanks } from '../../../db/data-access';
import { getUsername } from '../../util/request-helper';
import { badRequest, generateJsonResponse } from '../../util/response-helper';

type AddTierToRanksHandler = Handler<APIGatewayProxyEventV2, APIGatewayProxyResultV2>;

type AddTierToRanksRequest = {
  tierIndex: number
}

export const handler: AddTierToRanksHandler = async (event) => {
  console.log(event)

  if (!event.body) {
    return badRequest()
  }

  const username = getUsername(event)
  const position = event.pathParameters?.position as string
  const tierId = uuid()

  try {
    const { tierIndex } = JSON.parse(event.body) as AddTierToRanksRequest
    if (tierIndex < 0) {
      throw new Error('Index cannot be negative')
    }

    await addTierToRanks(
      username,
      position,
      tierId,
      tierIndex
    )
  } catch (ex) {
    console.log(ex)
    return badRequest()
  }

  return generateJsonResponse({
    body: {
      tierId
    }
  })
}