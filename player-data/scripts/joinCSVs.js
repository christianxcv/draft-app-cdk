/**
 * Use this script to join CSV data from the FantasyData projections and stats tables.
 * 
 * FantasyData doesn't allow direct downloading of the tables into CSVs so manual steps are required to
 * gather the information:
 * 
 * 1. The table is actually split into two HTML tables - one for the player IDs and names, and one for the stats
 * 2. Use an HTML table to CSV converter online to convert each subtable into a CSV.
 * 3. Save the CSVs into the player-data directory of this project.
 * 4. Update the filePath variables below to the newly saved CSV file names and run the script
 * 5. A new CSV will be created and saved to the file name of the  newFilePath variable
 * 
 */

const fs = require('fs'),
  path = require('path'),    
  namesFilePath = path.join(__dirname, '../data/2023-projections-names.txt'),
  statsFilePath = path.join(__dirname, '../data/2023-projections-stats.txt'),
  newFilePath = path.join(__dirname, '../out/2023-projections-out.csv');
  
const names = fs.readFileSync(namesFilePath, 'utf8');
const stats = fs.readFileSync(statsFilePath, 'utf8');

const namesArr = names.split('\n')
const statsArr = stats.split('\n')

const resultArr = []
for (let i = 0; i < namesArr.length; i++) {
  resultArr.push(`${namesArr[i]},${statsArr[i]}`)
}

console.log(resultArr)

const result = resultArr.join('\n')

fs.writeFileSync(newFilePath, result);