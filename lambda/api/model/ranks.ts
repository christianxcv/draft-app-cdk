export type Tier = {
  [tierNumber: number]: Array<string>
}

export type Ranks = {
  tiers: Tier
  tierOrder: Array<string>
}

export type RanksByPosition = {
  [position: string]: Ranks
}

export enum UserFlag {
  AllIn = "ALL_IN",
  Neutral = "NEUTRAL",
  AllOut = "ALL_OUT",
  Unranked = "UNRANKED"
}

export type UserFlags = {
  [playerId: string]: UserFlag
}