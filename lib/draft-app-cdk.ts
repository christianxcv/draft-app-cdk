import * as cdk from '@aws-cdk/core';

import { APP_PREFIX } from './constants';
import { CognitoStack } from './stacks/cognito';
import { DynamoStack } from './stacks/dynamo';
import { iamStack } from './stacks/iam';
import { LambdaStack } from './stacks/lambda';
import { RestApiStack } from './stacks/rest-apigw';
import { s3Stack } from './stacks/s3';

export type StackProps = {
  region: string
  accountId: string
}

const personalAccount: StackProps = {
  region: 'us-east-1',
  accountId: '383989225973'
}

export class DraftAppCdk {
  constructor(app: cdk.Construct) {
    const iam = new iamStack(app, `${APP_PREFIX}-iamStack`)
    const s3 = new s3Stack(app, `${APP_PREFIX}-s3Stack`)
    const dynamo = new DynamoStack(app, `${APP_PREFIX}-DynamoStack`)
    const cognito = new CognitoStack(app, `${APP_PREFIX}-CognitoStack`, {
      draftsTable: dynamo.draftsTable
    })
    const lambda = new LambdaStack(app, `${APP_PREFIX}-LambdaStack`, {
      draftsTable: dynamo.draftsTable
    })
    const restApi = new RestApiStack(app, `${APP_PREFIX}-RestApiStack`, {
      lambdaStack: lambda,
      userPool: cognito.userPool
    })
  }
}
