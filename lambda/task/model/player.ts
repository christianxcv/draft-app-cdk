import { PlayerAdp, Projection } from '../../api/model/player'

export type DraftLabPlayerModel = {
  playerId: number
  team: string
  number: number
  firstName: string
  lastName: string
  position: string
  fantasyPosition: string
  name: string
  age: number
  photoUrl: string
  byeWeek: number
  shortName: string
  adp?: number
  averageDraftPosition?: PlayerAdp
  projection?: Projection
}

export type PlayerIdToPlayerMap = { [playerId: string]: DraftLabPlayerModel }