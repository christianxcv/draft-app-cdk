import { APIGatewayProxyEventV2, APIGatewayProxyResultV2, Handler } from 'aws-lambda';

import { SUPPORTED_POSITIONS } from '../../../constant';
import { getRanks, getRanksByPosition } from '../../../db/data-access';
import { GetRanksResponse } from '../../model/response';
import { extractOptionStringParamFromQueryParams } from '../../util/request-helper';
import { generateJsonResponse } from '../../util/response-helper';

type GetPublicRanksHandler = Handler<APIGatewayProxyEventV2, APIGatewayProxyResultV2<GetRanksResponse>>

export const handler: GetPublicRanksHandler = async (event) => {
  console.log(event)
  const position = extractOptionStringParamFromQueryParams(event, 'position')
  const ranks = position && SUPPORTED_POSITIONS.includes(position)
    ? await getRanksByPosition(position)
    : await getRanks()

  return generateJsonResponse({
    body: ranks
  })
}