import { APIGatewayProxyEventV2 } from 'aws-lambda';

export const getUsername = (event: APIGatewayProxyEventV2) =>
  (event.requestContext as any).authorizer.jwt.claims['cognito:username']

export const extractOptionalNumberParamFromQueryParams = (event: APIGatewayProxyEventV2, paramName: string): number | undefined => {
  if (!event.queryStringParameters) {
    return undefined
  }

  const stringValue = event.queryStringParameters[paramName]
  if (!stringValue) {
    return undefined
  }

  const value = parseInt(stringValue)
  return isNaN(value) ? undefined : value
}

export const extractOptionStringParamFromQueryParams = (event: APIGatewayProxyEventV2, paramName: string): string | undefined => {
  if (!event.queryStringParameters) {
    return undefined
  }
  return event.queryStringParameters[paramName]
}