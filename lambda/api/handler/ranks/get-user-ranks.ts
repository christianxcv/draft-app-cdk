import { APIGatewayProxyEventV2, APIGatewayProxyResultV2, Handler } from 'aws-lambda';

import { SUPPORTED_POSITIONS } from '../../../constant';
import { getRanks as getRanksDB, getRanksByPosition as getRanksByPositionDB } from '../../../db/data-access';
import { GetRanksResponse } from '../../model/response';
import { extractOptionStringParamFromQueryParams, getUsername } from '../../util/request-helper';
import { generateJsonResponse } from '../../util/response-helper';

type GetUserRanksHandler = Handler<APIGatewayProxyEventV2, APIGatewayProxyResultV2<GetRanksResponse>>

export const handler: GetUserRanksHandler = async (event) => {
  console.log(event)
  const username = getUsername(event)
  const position = extractOptionStringParamFromQueryParams(event, 'position')
  const ranks = position && SUPPORTED_POSITIONS.includes(position)
    ? await getRanksForPosition(username, position)
    : await getRanks(username)
  return generateJsonResponse({
    body: ranks
  })
}

const getRanksForPosition = async (username: string, position: string): Promise<GetRanksResponse> => {
  const userRanks = await getRanksByPositionDB(position, username)
  if (Object.keys(userRanks.ranks).length) {
    return userRanks
  }

  const defaultRanks = await getRanksByPositionDB(position)
  return defaultRanks
}

const getRanks = async (username: string): Promise<GetRanksResponse> => {
  const userRanks = await getRanksDB(username)
  if (Object.keys(userRanks).length === SUPPORTED_POSITIONS.length) {
    return userRanks
  }

  const defaultRanks = await getRanksDB()
  return {
    ...defaultRanks,
    ...userRanks
  }
}