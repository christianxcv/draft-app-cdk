import { APIGatewayProxyEventV2, APIGatewayProxyResultV2, Handler } from 'aws-lambda';

import { getPlayerDetail } from '../../../db/data-access';
import { GetPlayerDetailResponse } from '../../model/response';
import { getUsername } from '../../util/request-helper';
import { generateJsonResponse } from '../../util/response-helper';

type GetDraftsHandler = Handler<APIGatewayProxyEventV2, APIGatewayProxyResultV2<GetPlayerDetailResponse>>

export const handler: GetDraftsHandler = async (event) => {
  console.log(event)
  const username = getUsername(event)
  const playerId = event.pathParameters?.playerId as string
  const response = await getPlayerDetail(username, playerId)
  console.log(response)
  return generateJsonResponse({
    body: response
  })
}